from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^tweettest/', include('tweettest.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'.*?', include('tweettest.urls')),
                       
)
