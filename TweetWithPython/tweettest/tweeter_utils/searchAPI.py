# -*- coding: utf-8 -*-

"""
Created on Sun Nov 2 12:02:38 2014
@author: Ivsiva
"""
import tweepy
import json
from tweepy import OAuthHandler
import imp
from django.conf import settings

c = imp.load_source('ctweet', 'tweettest/machine_learning/ctweet.py')

auth = OAuthHandler(settings.TWIT_CKEY, settings.TWIT_CSECRET)
auth.set_access_token(settings.TWIT_ATOKEN,settings.TWIT_ASECRET)
api = tweepy.API(auth)

def getPastTweets(keyword, sinceDate, untilDate, tweetcount, resultType):
    """
    Search for tweet data by the keyword parameter, filtered by the startDate
    and untilDate.

    @type  keyword: string
    @param keyword: The keyword by which the tweets are found
    @type  sinceDate: string
    @param sinceDate: The earliest date the tweets have been created
    @type  untilDate: string
    @param untilDate: The latest date the tweets have been created
    @type  tweetcount: number
    @param tweetcount: The maximum amount of tweets to be returned
    @type  resultType: string
    @param resultType: Specifies type of tweets
    @rtype:   list of dictionaries
    @return:  List of tweet data dictionaries
    """
    tweets_data = []
    query = keyword+" since:"+sinceDate+" until:"+untilDate
    for tweet in tweepy.Cursor(api.search,
                               q=query,
                               count=tweetcount,
                               result_type=resultType,
                               include_entities=True,
                               lang="en").items(tweetcount):
                                try:
                                    tweets_data.append(c.cTweet(tweet))
                                except AttributeError, e:
                                    print e
    return tweets_data


def getPastTweetsSafeToFile(filename, keyword, sinceDate, untilDate,
                            tweetcount, resultType):
    """
    Search for tweet data by the keyword parameter,
    filtered by the startDate and untilDate
    and save retrieved data to a json file

    @type  filename: string
    @param filename: The file path containing the location where the
        json tweet data is going to be stored
    @type  keyword: string
    @param keyword: The keyword by which the tweets are found
    @type  sinceDate: string
    @param sinceDate: The earliest date the tweets have been created
    @type  untilDate: string
    @param untilDate: The latest date the tweets have been created
    @type  tweetcount: number
    @param tweetcount: The maximum amount of tweets to be returned
    @type  resultType: string
    @param resultType: Specifies type of tweets
    @rtype:   list of dictionaries
    @return:  List of tweet data dictionaries
    """
    tweets_data = []
    query = keyword + " since:" + sinceDate + " until:" + untilDate
    with open(filename, "w") as outfile:
        pass
    output = "["
    for tweet in tweepy.Cursor(api.search,
                               q=query,
                               count=tweetcount,
                               result_type=resultType,
                               include_entities=True,
                               lang="en").items(tweetcount):
                                    try:
                                        tweets_data.append(c.cTweet(tweet))
                                    except AttributeError:
                                        print "Attribute error"
                                    with open(filename, 'a') as outfile:
                                        output += json.dumps(c.cTweet(tweet))
                                        output += ','
    output = output[0:-1]
    output += "]"
    with open(filename, 'a') as outfile:
        outfile.write(output)
    return tweets_data


def readSavedFileToTweets(filename):
    """
    Return tweet data dictionary from json file by specified filename

    @type  filename: string
    @param filename: The file path containing the location where the
        json tweet data is stored
    @rtype:   list of dictionaries
    @return:  List of tweet data dictionaries
    """
    with open(filename) as json_file:
        json_data = json.loads(json_file.read())
    # returns a dictionary, to use try:
    # json_data[{numberOfElement}][{"Attribute"}]
    return json_data
