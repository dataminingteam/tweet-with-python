# -*- coding: utf-8 -*-
"""
Created on Sat Nov 29 14:19:32 2014

@author: Ivsiva
"""
import datetime
import searchAPI as s


def testResultsLength():
    fourDaysAgo = datetime.timedelta(days=4)
    twoDaysAgo = datetime.timedelta(days=2)
    print("If results are the correct count")
    startDate = (datetime.datetime.now() - fourDaysAgo).strftime('%Y-%m-%d')
    untilDate = (datetime.datetime.now() - twoDaysAgo).strftime('%Y-%m-%d')
    importedtweets = len(s.getPastTweetsSafeToFile("data.txt",
                                                   "test",
                                                   startDate, untilDate, 19,
                                                   "recent"))
    collectedTweets = len(s.readSavedFileToTweets("data.txt"))
    assert importedtweets == collectedTweets


def testGenerateCorrectDatesSafeToFile():
    fourDaysAgo = datetime.timedelta(days=4)
    twoDaysAgo = datetime.timedelta(days=2)
    startDate = (datetime.datetime.now() - fourDaysAgo).strftime('%Y-%m-%d')
    untilDate = (datetime.datetime.now() - twoDaysAgo).strftime('%Y-%m-%d')
    assert len(s.getPastTweetsSafeToFile("data.txt", "test", startDate,
                                         untilDate, 19, "recent")) <= 19


def testGenerateWrongDatesSafeToFile():
    nineDaysAgo = datetime.timedelta(days=9)
    elevenDaysAgo = datetime.timedelta(days=11)
    startDate = (datetime.datetime.now() - elevenDaysAgo).strftime('%Y-%m-%d')
    untilDate = (datetime.datetime.now() - nineDaysAgo).strftime('%Y-%m-%d')
    assert len(s.getPastTweetsSafeToFile("data.txt", "test", startDate,
                                         untilDate, 19, "recent")) == 0


def testGenerateCorrectDates():
    fourDaysAgo = datetime.timedelta(days=4)
    twoDaysAgo = datetime.timedelta(days=2)
    startDate = (datetime.datetime.now() - fourDaysAgo).strftime('%Y-%m-%d')
    untilDate = (datetime.datetime.now() - twoDaysAgo).strftime('%Y-%m-%d')
    assert len(s.getPastTweets("test", startDate,
                               untilDate, 19, "recent")) <= 19


def testGenerateWrongDates():
    nineDaysAgo = datetime.timedelta(days=9)
    elevenDaysAgo = datetime.timedelta(days=11)
    startDate = (datetime.datetime.now() - elevenDaysAgo).strftime('%Y-%m-%d')
    untilDate = (datetime.datetime.now() - nineDaysAgo).strftime('%Y-%m-%d')
    assert len(s.getPastTweets("test", startDate,
                               untilDate, 19, "recent")) == 0
