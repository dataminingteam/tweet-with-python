# -*- coding: utf-8 -*-

from machine_learning.machine import Machine
from machine_learning.data_processing import DataSetPreparer
import machine_learning.ctweet as c
import tweeter_utils.searchAPI as s
from pprint import pprint

dsp = DataSetPreparer()
# s.getPastTweetsSafe("python","2014-11-25", "2014-11-29",10000,"recent")
# s.getPastTweetsSafeToFile("data_test.txt","python",
# "2014-11-30", "2014-12-01",1000,"recent")

# Preparing train set
array_train = s.readSavedFileToTweets('data/data_train.txt')
dataset_train = dsp.generateFeatureSet(array_train)
labelset_train = dsp.generateLabels(array_train)

# Preparing random forests machine
machine = Machine(dataset_train, labelset_train, "RandomForest")
result_train = machine.check(dataset_train)

print("Number of mislabeled points out of a total %d points : %d"
      % (len(dataset_train), (labelset_train != result_train).sum()))
pprint('Training done')

# Preparing test set
array_test = s.readSavedFileToTweets('data/data_test.txt')
dataset_test = dsp.generateFeatureSet(array_test)
labelset_test = dsp.generateLabels(array_test)
result_test = machine.check(dataset_test)


print("Number of mislabeled points out of a total %d points : %d"
      % (len(dataset_test), (labelset_test != result_test).sum()))

result1 = dsp.generateMatrixConf(labelset_train, result_train)
dsp.printMatrix(result1)
pprint("*****************")
result = dsp.groupLabels(labelset_test)
dsp.printLabels(result)
result2 = dsp.generateMatrixConf(labelset_test, result_test)
dsp.printMatrix(result2)


def estimateTwit(keyword, tweet, machineType):

    train_set_name = Machine.train_sets[keyword]
    array_train = s.readSavedFileToTweets(train_set_name)
    dataset_train = dsp.generateFeatureSet(array_train)
    labelset_train = dsp.generateLabels(array_train)

    c_machine = Machine(dataset_train, labelset_train, machineType)

    test_set_name = Machine.test_sets[keyword]
    array_test = s.readSavedFileToTweets(test_set_name)
    dataset_test = dsp.generateFeatureSet(array_test)
    labelset_test = dsp.generateLabels(array_test)

    features_tweet = dsp.generateFeatureSet([tweet])
    result = c_machine.check(features_tweet)
    pprint(result)
    score = c_machine.score(dataset_test, labelset_test)
    pprint(score)
    return (result, score)


estimateTwit("python", c.setProperties(1000, 1, 1, 1, 1), "RandomForest")
