# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import TweetForm1
from .forms import TweetForm2
from machine_learning.machine import Machine
from machine_learning.data_processing import DataSetPreparer
import machine_learning.ctweet as c
import tweeter_utils.searchAPI as searchTweet


def barchart(request):
    """
    barchart is a View that fetches data from
    Twitter and passes it to the machine learning
    classes and then generates a Bar Chart.
    It receives the input data from tweet2 page,
    passes it as a query to the Twitter API,
    (two queries one for training and one for testing)
    receives the resultset from Twitter,
    insert the resultset in the machine learning class,
    (one for training and one for testing)
    and sends the output data to bar_chart.html template.
    """
    if request.session.get('_twit_keyword', None) is None:
        print 'None'
        return HttpResponseRedirect('/tweet2')
    twitKeyword = str(request.session.get('_twit_keyword'))
    twitChosenMachine = str(request.session.get('_twit_chosenMachine'))
    twitSizeTrainSamples = int(request.session.get('_twit_sizeTrainSamples'))
    twitSizeTestSamples = int(request.session.get('_twit_sizeTestSamples'))
    twitStartDateTraining = str(request.session.get('_twit_startDateTraining'))
    twitEndDateTraining = str(request.session.get('_twit_endDateTraining'))
    twitStartDateTesting = str(request.session.get('_twit_startDateTesting'))
    twitEndDateTesting = str(request.session.get('_twit_endDateTesting'))
    request.session.flush()
    """
    Get the data from the POST method of tweet2 page
    and store it in local variables. Destroy the current
    session when data is passed from the session variable.
    """
    train_resultSet = searchTweet.getPastTweets(twitKeyword,
                                                twitStartDateTraining,
                                                twitEndDateTraining,
                                                twitSizeTrainSamples,
                                                "recent")
    """
    Search for tweets with the specified arguments
    passed from the POST method of tweet2 page and
    stores the resultset in the variable train_resultSet.
    """
    if not train_resultSet:
        return HttpResponse("The inserted arguments generated empty train list.")
    """
    searchTweet.getPastTweets will return a list if
    all the input parameters are valid and empty list if
    there is any invalid. If the train_resultSet variable is empty list
    then redirect to the main page.
    """
    dataGenerator = DataSetPreparer()
    train_dataset = dataGenerator.generateFeatureSet(train_resultSet)
    train_labelset = dataGenerator.generateLabels(train_resultSet)
    machine = Machine(train_dataset, train_labelset, twitChosenMachine)
    train_result_var = machine.check(train_dataset)
    """
    Creating an instance of data_processing.py class,
    generating feature set and label set with the data from Twitter and
    then creating Machine learning instance and train it with this data.
    """
    test_resultSet = searchTweet.getPastTweets(twitKeyword,
                                               twitStartDateTesting,
                                               twitEndDateTesting,
                                               twitSizeTestSamples,
                                               "recent")
    if not train_resultSet:
        return HttpResponse("The inserted arguments generated empty test list.")
    test_dataset = dataGenerator.generateFeatureSet(test_resultSet)
    test_labelset = dataGenerator.generateLabels(test_resultSet)
    test_result_var = machine.check(test_dataset)
    """
    Now predict based on the learning.
    """
    test_xLabelnames = []
    i = 0
    testMessage = ""
    message1 = ""
    message2 = ""
    highestScoreTest = 0
    for rs in test_resultSet:
        test_xLabelnames.append(i+1)
        if test_labelset[i] > highestScoreTest:
            highestScoreTest = test_labelset[i]
        if test_result_var[i] > highestScoreTest:
            highestScoreTest = test_result_var[i]
        message1 = message1 + str(test_labelset[i]) + ","
        message2 = message2 + str(test_result_var[i]) + ","
        i = i + 1
    testMessage = message1[:-1] + "|" + message2[:-1]
    """
    Storing the values in suitable format for
    django-chart-tools chart that is generated.
    """
    matrixConfTrain = dataGenerator.generateMatrixConf(train_labelset, train_result_var)
    matrixConfTest = dataGenerator.generateMatrixConf(test_labelset, test_result_var)
    result1 = dataGenerator.getMatrixConf(matrixConfTrain)
    result2 = dataGenerator.getMatrixConf(matrixConfTest)
    matrixLabels = dataGenerator.classes
    """
    Generating the Confusion matrices later dispayed in the barchart page.
    """
    return render_to_response("chart_tools/bar_chart.html", {
                              'testMessage': testMessage,
                              'highestScoreTest': highestScoreTest,
                              'xLabels': test_xLabelnames,
                              'title': twitKeyword,
                              'matrixConfTrain': zip(matrixLabels, result1),
                              'matrixConfTest': zip(matrixLabels, result2),
                              'matrixLabels': matrixLabels})
    """
    render_to_response is passing all the data
    to the bar_chart.html template.
    """


def index(request):
    """
    index is the view loaded at http://127.0.0.1:8000/
    It displays a form with 7 input fields
    required for predicting the expected number of
    favourite count.
    """
    if request.method == 'POST':
        form = TweetForm1(request.POST)
        if form.is_valid():
            keyword = form.cleaned_data['keyword']
            numOfHashtags = form.cleaned_data['numOfHashtags']
            numOfUrls = form.cleaned_data['numOfUrls']
            numOfPlaces = form.cleaned_data['numOfPlaces']
            numOfPictures = form.cleaned_data['numOfPictures']
            followersCount = form.cleaned_data['followersCount']
            machineType = form.cleaned_data['chosenMachine']
            request.session.flush()
            result, score = estimateTwit(keyword, c.setProperties(followersCount,
                                                                  numOfPictures,
                                                                  numOfPlaces,
                                                                  numOfUrls,
                                                                  numOfHashtags),
                                         machineType)
            score = score * 100
            result = str(result)
            score = str(score)
            message = "Your expected number of favourite count is about " + result + " with " + score + "% probability!"
            return render_to_response("result1.html", {'data': message})
        """
        If the form did pass the validations
        then get the data from the session and pass it
        to the estimation method and
        pass the result to the result1.html template.
        """
    else:
        form = TweetForm1()
    data = {'form': form}
    return render(request, 'tweet.html', data)
    """
    If the form did not pass the validation then
    return to the same page and display the errors.
    """


def tweet2(request):
    """
    tweet2 is the view that displays a form with
    8 fields required for generating the barchart.
    """
    if request.method == 'POST':
        form = TweetForm2(request.POST)
        if form.is_valid():
            keyword = form.cleaned_data['keyword']
            chosenMachine = form.cleaned_data['chosenMachine']
            sizeTrainSamples = form.cleaned_data['sizeTrainSamples']
            sizeTestSamples = form.cleaned_data['sizeTestSamples']
            startDateTraining = str(form.cleaned_data['startDateTraining'])
            endDateTraining = str(form.cleaned_data['endDateTraining'])
            startDateTesting = str(form.cleaned_data['startDateTesting'])
            endDateTesting = str(form.cleaned_data['endDateTesting'])
            request.session['_twit_keyword'] = keyword
            request.session['_twit_chosenMachine'] = chosenMachine
            request.session['_twit_sizeTrainSamples'] = sizeTrainSamples
            request.session['_twit_sizeTestSamples'] = sizeTestSamples
            request.session['_twit_startDateTraining'] = startDateTraining
            request.session['_twit_endDateTraining'] = endDateTraining
            request.session['_twit_startDateTesting'] = startDateTesting
            request.session['_twit_endDateTesting'] = endDateTesting
            return HttpResponseRedirect('/barchart')
            """
            If the form did pass the validations then
            it stores the input data to the global
            session variable and redirects to the barchart page.
            """
    else:
        form = TweetForm2()
    data = {'form': form}
    return render(request, 'tweet2.html', data)
    """
    If the form did not pass the validation then
    return to the same page and display the errors.
    """


def estimateTwit(keyword, tweet, machineType):
    """
    Method used to estimate the expected number
    of favourite count by reading data from a file.
    @param keyword: variable containing the tweet keyword
    @type: string (list of characters)
    @param tweet: variable holding the values of
    followersCount, numOfPictures, numOfPlaces, numOfUrls, numOfHashtags
    @type: list of ctweet parameters
    @param machineType: variable containing the type of
    the machine (SVM or RandomForest)
    @type: string (list of characters)
    @return: two integer variables
    """

    dsp = DataSetPreparer()
    train_set_name = Machine.train_sets[keyword]
    array_train = searchTweet.readSavedFileToTweets("tweettest/" + train_set_name)
    dataset_train = dsp.generateFeatureSet(array_train)
    labelset_train = dsp.generateLabels(array_train)
    c_machine = Machine(dataset_train, labelset_train, machineType)
    test_set_name = Machine.test_sets[keyword]
    array_test = searchTweet.readSavedFileToTweets("tweettest/" + test_set_name)
    dataset_test = dsp.generateFeatureSet(array_test)
    labelset_test = dsp.generateLabels(array_test)
    features_tweet = dsp.generateFeatureSet([tweet])
    result = c_machine.check(features_tweet)
    score = c_machine.score(dataset_test, labelset_test)
    return (result, score)
