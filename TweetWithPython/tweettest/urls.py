# -*- coding: utf-8 -*-
from django.conf.urls import patterns
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from tweettest import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name="tweet"),
                       url(r'^tweet2$', views.tweet2, name="tweet2"),
                       url(r'^barchart$', views.barchart, name='barchart'),
                       ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
"""
The urls that the website is currently supporting
http://127.0.0.1:8000/
http://127.0.0.1:8000/tweet2
http://127.0.0.1:8000/barchart - redirects to
tweet2 if session variable is empty
"""
