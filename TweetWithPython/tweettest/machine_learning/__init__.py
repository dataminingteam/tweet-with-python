"""
This module contains classes that process and prepare the data
(L{DataSetPreparer} and L{ctweet}), and later use the data
machine learning (L{machine}) operations.
"""
