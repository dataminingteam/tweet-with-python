# -*- coding: utf-8 -*-

import pytest
from .. import data_processing as dp
from .. import ctweet as c
import numpy as np

dsp = dp.DataSetPreparer()


def testIfGeneratesGoodFeatureArray():
    samples = generateGoodSample()
    result = dsp.generateFeatureSet(samples)
    assert len(result) == len(samples)


def testIfFeaturesFailWithBadSample():
    with pytest.raises(TypeError):
        samples = generateBadSample()
        dsp.generateFeatureSet(samples)


def testIfGeneratesGoodLabelsArray():
    samples = generateGoodSample()
    result = dsp.generateLabels(samples)
    assert len(result) == len(samples)


def testIfLabelsFailWithBadSample():
    with pytest.raises(TypeError):
        samples = generateBadSample()
        dsp.generateLabels(samples)


def testIfProperGroupingLabels():
    samples = generateGoodLabelsSample()
    result = dsp.groupLabels(samples)
    assert len([i for i in result if i != 0]) == len(samples)


def testIfGroupingLabelsFailsWithBadSample():
    samples = generateBadLabelsSample()
    result = dsp.groupLabels(samples)
    assert len([i for i in result if i != 0]) == 1


def testIfGeneratingMatrWorksWithGoddSample():
    samples = generateGoodLabelsSample()
    result = dsp.generateMatrixConf(samples, samples)
    assert len([i for i in np.nditer(result) if i != 0]) == len(samples)


def testIfFailingGenerateMatrixWithBadSample():
    with pytest.raises(IndexError):
        samples = generateGoodLabelsSample()
        samples.append(10)
        samples2 = generateGoodLabelsSample()
        dsp.generateMatrixConf(samples, samples2)


def testIfPrintsLabels():
    samples = generateGoodLabelsPrintingSample()
    dsp.printLabels(samples)
    # succed if no exception
    assert True


def testIfDoesntPrintLabels():
    with pytest.raises(IndexError):
        samples = generateBadLabelsPrintingSample()
        dsp.printLabels(samples)


def testIfPrintsMatrix():
    samples = generateGoodMatrixPrintingSample()
    dsp.printLabels(samples)
    # succed if no exception
    assert True


def testIfDoesntPrintMatrix():
    with pytest.raises(IndexError):
        samples = generateBadMatrixPrintingSample()
        dsp.printLabels(samples)


def generateGoodMatrixPrintingSample():
    m_max = max(dsp.classes)
    return np.arange((m_max + 1)*(m_max + 1)).reshape(m_max + 1, m_max + 1)


def generateBadMatrixPrintingSample():
    m_max = max(dsp.classes)
    return np.arange((m_max) * (m_max)).reshape(m_max, m_max)


def generateGoodLabelsPrintingSample():
    return [i for i in range(0, max(dsp.classes) + 1)]


def generateBadLabelsPrintingSample():
    return [i for i in range(1, max(dsp.classes))]


def generateGoodLabelsSample():
    return [i for i in dsp.classes]


def generateBadLabelsSample():
    return [str(i) for i in dsp.classes]


def generateGoodSample():
    tweet1 = c.setAllProperties(1000, 1, 1, 1, 1, 100)
    tweet2 = c.setAllProperties(500, 0, 0, 1, 1, 25)
    return [tweet1, tweet2]


def generateBadSample():
    return [500, 100]
