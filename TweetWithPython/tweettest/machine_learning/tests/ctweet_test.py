# -*- coding: utf-8 -*-

from .. import ctweet as c


def testIfCreatesProperTweet():
    tweet = createGoodSample()
    result = c.setProperties(tweet['followers_count'], tweet['media'],
                             tweet['place'], tweet['url'],
                             tweet['hashtags'])
    tweet['favorite_count'] = 0
    assert result.items() == tweet.items()


def testIfCreateProperWholeTweet():
    tweet = createGoodSample()
    result = c.setAllProperties(tweet['followers_count'], tweet['media'],
                                tweet['place'], tweet['url'],
                                tweet['hashtags'], tweet['favorite_count'])
    assert result.items() == tweet.items()


def createGoodSample():
    return dict(created_at=0, place=1, url=3, hashtags=2,
                user_mentions=0, media=4, followers_count=200,
                retweet_count=0, favorite_count=50)
