# -*- coding: utf-8 -*-
"""
This module is designed to convert various inputs
to ctweet (a dictionary) form.
"""

import time
from pprint import pprint


def cTweet(twit=None):
        """
        This method transforms single tweet to custom cTweet dict structure
        @return: dict of created_at, place, url, hashtags,
            user_mentions, media, followers_count, retweet_count
        """

        ctweet = dict(created_at=0, place=0, url=0, hashtags=0,
                      user_mentions=0, media=0, followers_count=0,
                      retweet_count=0, favorite_count=0)

        if twit is None:
            return ctweet

        ctweet['created_at'] = time.mktime(twit.created_at.timetuple())

        if (twit.place is not None):
            ctweet['place'] = 1
        else:
            ctweet['place'] = 0

        ctweet['retweet_count'] = twit.retweet_count

        try:
            if(twit.favorite_count is not None):
                ctweet['favorite_count'] = twit.favorite_count
            else:
                ctweet['favorite_count'] = 0
        except Exception, e:
            pprint(e)
            ctweet['favorite_count'] = 0

        try:
            if(twit.entities['urls'] is not None):
                ctweet['url'] = len(twit.entities['urls'])
            else:
                ctweet['url'] = 0
        except Exception, e:
            pprint(e)
            ctweet['url'] = 0

        try:
            if(twit.entities['hashtags'] is not None):
                ctweet['hashtags'] = len(twit.entities['hashtags'])
            else:
                ctweet['hashtags'] = 0
        except Exception, e:
            pprint(e)
            ctweet['hashtags'] = 0

        try:
            if(twit.entities['user_mentions'] is not None):
                ctweet['user_mentions'] = len(twit.entities['user_mentions'])
            else:
                ctweet['user_mentions'] = 0
        except Exception, e:
            pprint(e)
            ctweet['user_mentions'] = 0

        try:
            if(twit.entities['media'] is not None):
                ctweet['media'] = len(twit.entities['media'])
            else:
                ctweet['media'] = 0
        except Exception, e:
            # pprint(e)
            ctweet['media'] = 0

        ctweet['followers_count'] = twit.user.followers_count
        return ctweet


def setProperties(followers, media, place, urls, hashtags):
        """
        This method returns a cTweet dictionary, filling it with input values.
        """
        ctweet = dict(created_at=0, place=0, url=0, hashtags=0,
                      user_mentions=0, media=0, followers_count=0,
                      retweet_count=0, favorite_count=0)
        ctweet['place'] = place
        ctweet['url'] = urls
        ctweet['hashtags'] = hashtags
        ctweet['media'] = media
        ctweet['followers_count'] = followers
        return ctweet


def setAllProperties(followers, media, place, urls, hashtags, favorite_count):
        """
        This method returns a cTweet dictionary, filling it with input values.
        """
        ctweet = dict(created_at=0, place=0, url=0, hashtags=0,
                      user_mentions=0, media=0, followers_count=0,
                      retweet_count=0, favorite_count=0)
        ctweet['place'] = place
        ctweet['url'] = urls
        ctweet['hashtags'] = hashtags
        ctweet['media'] = media
        ctweet['followers_count'] = followers
        ctweet['favorite_count'] = favorite_count
        return ctweet
