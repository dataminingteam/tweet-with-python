# -*- coding: utf-8 -*-

"""
This class is a wrapper for two machine
learning classifiers - SVM or Random Forest.
Typical usage:
>>> X = [[0,2], [1,1], [2,0]]
>>> Y = [1, 0, 1]
>>> m = Machine(X, Y, "SVM")
>>> m.check([2,2])
array([1])
"""
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier


class Machine:
    train_sets = dict(python="data/data_train.txt")
    test_sets = dict(python="data/data_test.txt")
    machines = dict(SVM=svm.SVC(cache_size=3000.0, class_weight=None),
                    RandomForest=RandomForestClassifier(n_estimators=2,
                                                        max_features=None))
    """
    the dictionary containing constructors of different classifiers
    """

    def __init__(self, FeaturesArray, LabelsArray, machineType):
        """
        This constructor creates an instance of SVM or
        RandomForest (depending on isSVM parameters)
        and already creates model based on input arrays.
        Usage:

        >>> X = [[0,1, 2], [1,1, 1], [2,1, 0]]
        >>> Y = [1, 0, 1]
        >>> m = Machine(X, Y, "RandomForest")

        @param FeaturesArray: array containg samples
        @type FeaturesArray: numpy array of size
            (numberOfSamples x numberOfFeatures)
        @param LabelsArray: array containg labels
            for samples (one for each sample)
        @type LabelsArray: numpy array of size (numberOfSamples x 1)
        @param machineType: one of a key of L{machines}
        @type machineType: string
        """

        self.machine = self.machines[machineType]
        self.machine.fit(FeaturesArray, LabelsArray)

    def check(self, twit):
        """
        This method returns an estimated label(s) for
        the given input of sample, based on previously created model.
        Usage:
            >>> X = [[0, 2, 2], [1, 1, 1], [2, 1, 2]]
            >>> Y = [1, 0, 1]
            >>> m = Machine(X, Y, "SVM")
            >>> m.check([2,2,2])
            array([1])

        @param twit: a sample twit(s)
        @type twit: numpy array with samples
        @return: Label of class associated with the sample
        """
        return self.machine.predict(twit)

    def score(self, X_test, Y_test):
        """
        Calculates the accuracy of models
        return: number from 0 to 1 representing the accuracy
        """
        return self.machine.score(X_test, Y_test)

    @staticmethod
    def getKeywords():
        """
        @return: the list of possible keywords
        """
        keys1 = Machine.test_sets.keys()
        keys2 = Machine.train_sets.keys()
        return [i for i in keys1 if i in keys2]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
