# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_iris
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
from data_processing import DataSetPreparer
from .. import tweeter_utils as s
from pprint import pprint
import imp

# s = imp.load_source('searchAPI', '../tweeter_utils/searchAPI.py')

# Found on 
# http://scikit-learn.org/dev/auto_examples/svm/plot_rbf_parameters.html#example-svm-plot-rbf-parameters-py


##############################################################################
# Load and prepare data set
#
# dataset for grid search
dg = DataSetPreparer()

array_train = s.readSavedFileToTweets('../data/python_train.txt')
X = dg.generateFeatureSet(array_train)
Y = dg.generateLabels(array_train)

# It is usually a good idea to scale the data for SVM training.
# We are cheating a bit in this example in scaling all of the data,
# instead of fitting the transformation on the training set and
# just applying it on the test set.

C_range = 10.0 ** np.arange(-2, 9)
gamma_range = 10.0 ** np.arange(-5, 4)
param_grid = dict(gamma=gamma_range, C=C_range)
cv = StratifiedKFold(y=Y, n_folds=3)
grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv)
grid.fit(X, Y)

print("The best classifier is: ", grid.best_estimator_)

plt.figure(figsize=(8, 6))
plt.subplots_adjust(left=0.05, right=0.95, bottom=0.15, top=0.95)
plt.imshow(scores, interpolation='nearest', cmap=plt.cm.spectral)
plt.xlabel('gamma')
plt.ylabel('C')
plt.colorbar()
plt.xticks(np.arange(len(gamma_range)), gamma_range, rotation=45)
plt.yticks(np.arange(len(C_range)), C_range)

plt.show()