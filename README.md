# README #

Project for course at DTU called Data Mining in Python.
Project is designed to estimate number of retweets and answers to particular tweets, while basing on various data as number of followers, gender and content of tweet. 

### What is this repository for? ###

* Version 0.0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
Please, do not contribute!

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Or anyone in dataminingteam.